import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Family, FamilyMember } from '../../models';
import { FamilyService } from '../../services/family.service';

@Component({
  selector: 'lab-js-add-family',
  templateUrl: './add-family.component.html',
  styleUrls: ['./add-family.component.scss']
})
export class AddFamilyComponent implements OnInit {
  public familyForm: FormGroup;
  public family: Family;
  public isEditingMode: boolean;
  public get children() {
    return this.familyForm.get('children') as FormArray;
  }
  public constructor(
    private readonly familyService: FamilyService,
  ) {
    this.family = history.state?.family
    this.isEditingMode = !!this.family?.id
  }
  public ngOnInit(): void {
    this.familyForm = new FormGroup({
      id: new FormControl(this.family?.id || null, []),
      name: new FormControl(this.family?.name || '', [Validators.required]),
      father: this.generateFamilyMember(this.family?.father || undefined),
      mother: this.generateFamilyMember(this.family?.mother || undefined),
      children: new FormArray(this.family?.children.map(child=> this.generateFamilyMember(child))||[]),
    })
    if (!this.isEditingMode) this.addChild()
  }

  public generateFamilyMember (member?: FamilyMember) {
    return new FormGroup({
      name: new FormControl(member?.name||'', [Validators.required]),
      age: new FormControl(member?.age||0, [Validators.required])
    })
  }
  public addChild () {
    this.children.push(this.generateFamilyMember())
  }
  public removeChild (index: number) {
    this.children.removeAt(index)
  }
  public submit () {
    if(this.isEditingMode){
      this.familyService.editFamily$(this.familyForm.value).subscribe()
    } else {
      this.familyService.addFamily$(this.familyForm.value).subscribe()
    }
  }
}
